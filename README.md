
[![build status](https://gitlab.com/bitfireAT/dav4android/badges/master/build.svg)](https://gitlab.com/bitfireAT/dav4android/commits/master)


# dav4android

dav4android is an Android WebDAV/CalDAV/CardDAV library which has
primarily been developed for [DAVdroid](https://www.davdroid.com).

It's not intended as a general WebDAV framework for all kinds of
applications, but you may find it useful to fork and adapt it
to your needs.

Generated KDoc: https://bitfireAT.gitlab.io/dav4android/dokka/dav4android/


## License 

Copyright (C) bitfire web engineering (Ricki Hirner, Bernhard Stockmann).

This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome
to redistribute it under the conditions of the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).

